const CoinHive = require('coin-hive');

// This opens a chromium instance, so don't forget to close that when you're done mining
(async () => {
  let args = process.argv;
  let public_key = 'BWvUaXgDvxcYgHeHYU4BY7pFhiAkWGqo' // Coin-Hive's Site Key
  let opts = {};
  
  if(args.length > 2) {
    public_key = args[2]; // 1st parameter - siteKey
  }

  if(args.length > 3) {
    opts.username = parseInt(args[3]); // 2nd parameter - userName
  }

  // Create miner
  const miner = await CoinHive(public_key, opts); // Username

  // Setting Miner
  await miner.rpc('setThrottle', [0.2]); // Throttle, 0.0 for full power

  // Start miner
  await miner.start();

  // Listen on events
  miner.on('found', () => console.log('Found!'))
  miner.on('accepted', () => console.log('Accepted!'))
  miner.on('update', data => console.log(`
    Hashes per second: ${data.hashesPerSecond}
    Total hashes: ${data.totalHashes}
    Accepted hashes: ${data.acceptedHashes}
  `));
})();